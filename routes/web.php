<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/pusher', 'PusherController@index');
Route::post('/pusher/send', 'PusherController@sendPush');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
