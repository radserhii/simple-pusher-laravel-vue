@extends('layouts.app')

@section('content')
    <pusher-component app-key="{{ env('PUSHER_APP_KEY') }}"></pusher-component>
@endsection