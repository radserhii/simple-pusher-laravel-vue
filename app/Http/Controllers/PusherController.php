<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherController extends Controller
{
    public function index()
    {
        return view('pusher.index');
    }

    public function sendPush(Request $request)
    {
        $options = [
            'cluster' => 'eu',
            'encrypted' => true
        ];

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $data['username'] = $request->username;

        try {
            $pusher->trigger('channel', 'username', $data);
            return response()->json('Message was sent', 200);
        } catch (PusherException $e) {
            return response()->json($e->getMessage());
        }
    }
}
